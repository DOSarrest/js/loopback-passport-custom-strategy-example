# Loopback Passport Custom Strategy Example

This project provides an example of how to create a custom passport.js strategy for use with loopback.io.

This example is currently set up to send a HTTP request to a configured URL along with an access token to retrieve a user account.

## Providers.js Example
```json
{
    "custom-example": {
        "authScheme": "custom",
        "provider": "custom-example",
        "module": "loopback-passport-custom-strategy-example",
        "authPath": "/auth/example",
        "example": {
            "id": "123",
            "username": "example",
            "email": "example@email.com",
            "emailVerified": true
        },
        "authOptions": {
          "successRedirect": "/auth/success",
          "failureRedirect": "/auth/failure"
        },
        "passReqToVerify": true
    }
}
```
