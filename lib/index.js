'use strict';

const passport = require('passport-strategy');
const util = require('util');

/*
 * Config Defaults
 * passReqToCallback
 * @param options Provider options
 * @param verify Verification callback
 */
function Strategy(options, verify) {
  let me = this;
  if (typeof options === 'function') {
    verify = options;
    options = {};
  }
  if (!verify) {
    throw new TypeError('Strategy requires a verify callback');
  }
  passport.Strategy.call(me);
  me._options = options;
  me._verify = verify;
  me.name = 'custom-example';
  /**
   * Gets user details
   */
  me.getUserDetails = () => {
    return new Promise((resolve) => {
      resolve(me._options.example);
    });
  };
}
util.inherits(Strategy, passport.Strategy);

/*
 * Authenticate request
 * @param req HTTP Request Object
 * @param options AuthOptions
 */
Strategy.prototype.authenticate = function authenticate(req, options) {
  let me = this;
  options = options || {};
  function verified(err, user, info) {
    if (err) { return me.error(err); }
    if (!user) { return me.fail(info); }
    me.success(user, info);
  }
  try {
    me.getUserDetails().then(user => {
      if (me._options.passReqToVerify) {
        this._verify(req, user, verified);
      } else {
        this._verify(user, verified);
      }
    }).catch(err => {
      me.fail(err);
    });
  } catch (err) {
    me.error(err);
  }
};
module.exports = Strategy;
